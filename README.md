# Exponentielle Suche
Eine Seilbahnfahrt von der Talstation exponentielles Wachstum zur Bergstation exponentielle Suche - mit einem Zwischenstopp bei der berühmten Mittelstation 
binäre Suche. Unterwegs geniessen wir den atemberaubenden Einblick in die tiefe Schlucht der linearen Suche, in der Legenden zufolge, schon so manch Suchender auf ewig verschollen ist.

Wir starten mit den SuS bei einem intuitiven Verständnis des exponentiellen Wachstums sowie der Laufzeitkomplexität von vergleichsbasierten Algorithmen. Von hier aus bewegen wir uns über die lineare Suche zur binären Suche. Unterwegs analysieren und vergleichen die SuS die verschiedenen Algorithmen hinsichtlich ihrer Laufzeitkomplexität. 

Als finalen Auftrag entwerfen und analysieren die SuS die exponentielle Suche. Hiermit schliesst sich der Kreis zum Ausgangspunkt der LPU. Die SuS erkennen den ersten Schritt der exponentiellen Suche als konzeptuelle Umkehrung des exponentiellen Wachstums und den zweiten Schritt als die bekannte binäre Suche in einem endlichen Suchraum.

Die SuS erreichen erschöpft aber zufrieden das Ziel dieser LPU: Entwurf und Analyse eines Algorithmus zum effizienten Suchen auf unendlich langen, sortierten Listen.

```
git clone --depth 1 https://gitlab.ethz.ch/abz/fd-lpu/fs23/exponentielle-suche.git
```